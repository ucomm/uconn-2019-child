<?php

use UConn2019\Lib\Customizer;
?>

<footer id="footer-main">
    <div id="footer-decoration"></div>
    <div class="footer-main-wrap contain">

        <!-- Social Row -->
        <div class="footer-main-row social-row">
            <?php
            $socialFooterInfo = Customizer::$social;

            foreach ($socialFooterInfo as $index => $info) {
                $count = $index + 1;
                $link = get_theme_mod('footer_social_link_' . $count, $info['link']);
                $icon = get_theme_mod('footer_social_link_icon_' . $count, $info['iconClass']);
                $aria = get_theme_mod('footer_social_link_aria_' . $count, $info['socialType']);

            ?>
                <a href="<?php echo $link; ?>" target="_blank" rel="noreferrer"><i class="<?php echo $icon; ?>" aria-hidden="true"></i><span class="screen-reader"><?php echo $aria ?></span></a>
            <?php
            }
            ?>
        </div>
        <!-- /Social Row -->

        <!-- Link Columns -->
        <div class="footer-main-row">
            <div class="uc_footer_sidebar uc_footer_left_sidebar">
                <?php if (is_active_sidebar('uc_footer_left')) : ?>
                    <?php dynamic_sidebar('uc_footer_left'); ?>
                <?php endif; ?>
            </div>

            <div class="uc_footer_sidebar uc_footer_middle_sidebar">
                <?php if (is_active_sidebar('uc_footer_middle')) : ?>
                    <?php dynamic_sidebar('uc_footer_middle'); ?>
                <?php endif; ?>
            </div>

            <div class="uc_footer_sidebar uc_footer_right_sidebar">
                <?php if (is_active_sidebar('uc_footer_right')) : ?>
                    <?php dynamic_sidebar('uc_footer_right'); ?>
                <?php endif; ?>
            </div>
        </div>
        <!-- /Link Columns -->


        <!-- Meta Footer -->
        <div class="footer-main-row bottom-row">
            <div class="uc_footer_sidebar uc_footer_bottom_left_sidebar">
                <?php if (is_active_sidebar('uc_footer_bottom_left')) : ?>
                    <?php dynamic_sidebar('uc_footer_bottom_left'); ?>
                <?php endif; ?>
            </div>

            <div class="uc_footer_sidebar uc_footer_bottom_right_sidebar">
                <?php if (is_active_sidebar('uc_footer_bottom_right')) : ?>
                    <?php dynamic_sidebar('uc_footer_bottom_right'); ?>
                <?php endif; ?>
            </div>
        </div>
        <!-- /Meta Footer -->
        <div class="footer-compliance-wrapper">
            <ul class="footer-compliance-menu">
                <li class="footer-compliance-item">
                    <a href="https://uconn.edu" class="footer-compliance-text footer-compliance-link">UConn Home</a>
                </li>
                <li class="footer-compliance-item">
                    <a href="https://uconn.edu/disclaimers-privacy-copyright/" class="footer-compliance-text footer-compliance-link">Disclaimers, Privacy, & Copyright</a>
                </li>
                <li class="footer-compliance-item">
                    <a href="https://accessibility.uconn.edu/" class="footer-compliance-text footer-compliance-link">Accessibility</a>
                </li>
                <li class="footer-compliance-item">
                    <p class="footer-compliance-text">© <?php echo date('Y'); ?> University of Connecticut</p>
                </li>
            </ul>
        </div>
    </div>
</footer>