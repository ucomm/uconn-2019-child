<?php

use UConn2019Child\Frontend\SiteManagement;

$info = SiteManagement::getSiteInfo([
    'name',
    'description',
    'url'
]);

?>
<div id="uconn-banner">
    <div id="home-link-container">
        <a id="home-link" href="<?php echo home_url(); ?>">
            <span id="wordmark" aria-label="University of Connecticut">UConn</span>
            <span id="wordmark-location"><?php echo $info['description']; ?></span>
        </a>
    </div>
    <div id="button-container">
        <div class="icon-container" id="icon-container-search">
            <a class="btn" id="uconn-search" href="<?php echo $info['url']; ?>/search">
                <span class="no-css">Search <?php echo $info['name'] . ' ' . $info['description']; ?></span>
                <i class="icon-search" aria-hidden="true"></i>
            </a>
            <!-- Content provided by css -->
            <div id="uconn-search-tooltip" data-search-text="Search UConn <?php echo $info['description']; ?>"></div>
        </div>
        <div class="icon-container" id="icon-container-az">
            <a class="btn" id="uconn-az" href="https://uconn.edu/az">
                <span class="no-css">A to Z Index</span>
                <i class="icon-a-z" aria-hidden="true"></i>
            </a>
            <!-- Content provided by css -->
            <div id="uconn-az-tooltip"></div>
        </div>
        <div class="navigation-mobile">
            <button class="mobile-trigger">Menu</button>
            <div class="mobile-navigation-container">
                <div class="mobile-navigation-popup">
                    <button class="mobile-navigation-close">
                        <span class="screen-reader">Close mobile nav menu</span>
                        <i class="fas fa-times"></i>
                    </button>
                    <div class="mobile-navigation-menu-wrap">
                        <?php UConn2019\Lib\Menus::display_mobile_navigation(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>