<?php
/*
Template Name: Search Page
*/

if (!isset($helpers) || !class_exists('UConn2019\Lib\Helpers')) {
  include get_template_directory() . '/lib/Helpers.php';
  $helpers = new \UConn2019\Lib\Helpers();
}

use UConn2019Child\Frontend\Search;

add_filter('body_class', array('UConn2019\Lib\Helpers', 'add_angled_header_class'));
get_header();

?>

<main role="main" id="main-content">
  <?php echo $helpers->get_angled_header('Search'); ?>
  <section class="search-contain">
    <?php
    the_post();
    get_search_form(Search::createSearchFormArgs());
    ?>
    <div id="local-search-wrapper">
      <h2 style="display: none;" id="search-header">Search Results for <span id="search-query"></span></h2>
      <div id="search-results"></div>
    </div>
    <div id="cse-search-results">
      <gcse:searchresults-only></gcse:searchresults-only>
    </div>
    <div>
      <p id="search-error" style="display: none;"><strong>An error occurred while performing your search</strong></p>
      <button id="search-more-button" style="display: none;">Get More Results</button>
    </div>
    <div id="people-search-wrapper" class="hidden">
      <div>
        <a href="<?php echo home_url('/about/campus-directory'); ?>" class="button">Campus Directory</a>
      </div>
      <div>
        <a href="https://phonebook.uconn.edu" class="button">UConn Phonebook</a>
      </div>
    </div>
  </section>
</main>

<?php get_footer(); ?>