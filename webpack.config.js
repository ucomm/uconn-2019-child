const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin')

const prodEnv = process.env.NODE_ENV === 'production' ? true : false
const mode = prodEnv ? 'production' : 'development'
const buildDir = prodEnv ? path.resolve(__dirname, 'dist') : path.resolve(__dirname, 'build')
const optimization = prodEnv ? 
  {
    minimize: true,
    minimizer: [
      new CssMinimizerPlugin(),
      '...'
    ]
  } : {}

module.exports = {
  entry: {
    main: path.resolve(__dirname, 'src', 'js', 'index.ts'),
    search: path.resolve(__dirname, 'src', 'js', 'search.ts')
  },
  output: {
    path: buildDir,
    filename: '[name].js',
    chunkFilename: '[name].js'
  },
  mode,
  optimization,
  devtool: prodEnv ? 'source-map' : 'eval',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@babel/preset-env',
                  {
                    debug: true,
                    useBuiltIns: 'usage',
                    corejs: 3.13
                  }
                ],
              ]
            }
          }
        ]
      },
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: 'ts-loader'
      },
      /**
       * 
       * Use this if you intend to bundle style assets as well.
       * Make sure to install the appropriate loaders etc...
       * 
       */
      {
        test: /\.s?css$/i,
        exclude: /node_modules/,
        use: [
          MiniCssExtractPlugin.loader,
          { 
            loader: 'css-loader', 
            options: { 
              sourceMap: true,
              importLoaders: 2
            } 
          },
          {
            loader: 'postcss-loader'
          },
          { 
            loader: 'sass-loader', 
            options: { 
              sourceMap: true 
            } 
          }
        ]
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx', '.css', '.scss']
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css'
    })
  ],
}