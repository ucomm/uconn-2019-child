<?php

use UConn2019Child\Admin\PostTypes\CustomTypes;
use UConn2019Child\Assets\ScriptLoader;
use UConn2019Child\Assets\StyleLoader;
use UConn2019Child\Search\Search;

// Constants
define( 'UCONN_2019_PARENT_DIR', get_template_directory());
define( 'UCONN_2019_PARENT_URL', get_template_directory_uri());
define( 'UCONN_2019_CHILD_DIR', get_stylesheet_directory() );
define( 'UCONN_2019_CHILD_URL', get_stylesheet_directory_uri() );

// require files from lib
require_once('lib/Assets/Loader.php');
require_once('lib/Assets/ScriptLoader.php');
require_once('lib/Assets/StyleLoader.php');
require_once('lib/Admin/PostTypes/CustomTypes.php');
require_once('lib/Frontend/SiteManagement.php');
require_once('lib/Frontend/Search.php');
require_once('lib/Search/Search.php');

// Enqueue Scripts and Styles
$scriptLoader = new ScriptLoader();
$styleLoader = new StyleLoader();

if (!is_admin()) {
	$scriptLoader->enqueueAssets();
	$styleLoader->enqueueAssets();
} 

add_action('admin_menu', [ CustomTypes::class, 'hidePodcastPosts' ]);

// allow both logged in and anonymous users to search
add_action('wp_ajax_search', [ Search::class, 'searchHandler' ]);
add_action('wp_ajax_nopriv_search', [ Search::class, 'searchHandler' ]);