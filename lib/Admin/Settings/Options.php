<?php

namespace UConn2019Child\Admin\Settings;

/**
 * This class allows for 
 * - the creation of settings in the wp_options table
 * - getting and displaying the form to modify settings.
 */
class Options
{
  private $settingsName;

  public function __construct()
  {
    $this->settingsName = 'boilerplate-settings';
  }

  public function init()
  {
    add_action('admin_init', [$this, 'prepareSettingsSections']);
  }

  public function prepareSettingsSections()
  {
    $this->registerSettings();
    $this->addSettingsSection();
  }

  public function registerSettings()
  {
    register_setting($this->settingsName, $this->settingsName);
  }

  public function addSettingsSection()
  {
    add_settings_section(
      'boilerplate-section',
      __('Boilerplate Settings', 'boilerplate'),
      [$this, 'getBoilerplateSettingSection'],
      $this->settingsName
    );
  }

  public function getBoilerplateSettingSection()
  {
    $settings = $this->getSettings();
    include UCONN_2019_CHILD_DIR . 'partials/admin/settings-section.php';
  }

  public function getSettings()
  {
    return $this->prepareSettings();
  }

  protected function prepareSettings()
  {
    if (!get_option($this->settingsName)) {
      update_option($this->settingsName, [
        'boilerplate-string' => 'hello from the database',
        'boilerplate-array' => []
      ]);
    }
    return get_option($this->settingsName);
  }
}
