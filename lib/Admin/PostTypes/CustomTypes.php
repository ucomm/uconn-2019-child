<?php

namespace UConn2019Child\Admin\PostTypes;

class CustomTypes {
  public static function hidePodcastPosts() {
    remove_menu_page('edit.php?post_type=uconn360-podcast');
  }
}