<?php

namespace UConn2019Child\Frontend;

class Search {
  public static function createSearchFormArgs(): array {
    $value = '';
    if (isset($_REQUEST['s'])) {
      $value = strip_tags($_REQUEST['s']);
      $value = sanitize_text_field($value);
    }
    return [
      'description' => get_bloginfo('description'),
      'value' => $value
    ];
  }
}