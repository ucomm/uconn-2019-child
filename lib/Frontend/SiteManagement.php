<?php

namespace UConn2019Child\Frontend;

class SiteManagement {
  public static function getSiteInfo(array $infoToFind): array {
    return array_combine(
      array_values($infoToFind),
      array_map(function ($item) {
        return get_bloginfo($item);
      }, $infoToFind)
    );
  }
}