<?php

namespace UConn2019Child\Assets;

use UConn2019Child\Assets\Loader;

/**
 * A class to handle loading JS assets
 */
class ScriptLoader extends Loader
{
    /**
     * Enqueue the prepared scripts.
     * In this example, the script will only be enqueued if the shortcode is present.
     *
     * @return void
     */
    public function enqueue()
    {
        global $wp;
        $this->prepareAppScript();

        $template = get_page_template();

        if (false !== stripos($template, 'searchpage.php') || is_search()) {

            $customSearchData = [
                'cx' => isset($_GET['cx']) ? sanitize_text_field($_GET['cx']) : '004595925297557218349:65_t0nsuec8',
                'homeURL' => home_url(),
                'requestURL' => add_query_arg($wp->query_string, '', home_url($wp->request)),
                'wpAJAX' => admin_url('admin-ajax.php'),
                'wpSearchNonce' => wp_create_nonce('search-nonce')
            ];

            $customSearchObj = 'const customSearch = ' . json_encode($customSearchData);


            $this->prepareSearchScript();
            wp_enqueue_script($this->handle . '-search');
            wp_add_inline_script($this->handle . '-search', $customSearchObj, 'before');
        }
        
        wp_enqueue_script($this->handle);
    }

    /**
     * This method can be used to enqueue an asset on an admin page.
     * Use the slug to filter which pages it should be used on.
     *
     * @param string $hook - the admin page's slug to enqueue on
     * @return void
     */
    public function adminEnqueue(string $hook)
    {
    }

    /**
     * Prepare the script by registering it.
     *
     * @return void
     */
    private function prepareAppScript()
    {
        // enqueueing the parent script automatically enqueues the a11y-menu script
        $scriptDeps = [ $this->parentScriptHandle ];
        wp_register_script(
            $this->handle,
            UCONN_2019_CHILD_URL . $this->buildDir . '/main.js',
            $scriptDeps,
            false,
            true
        );
    }

    private function prepareSearchScript() {
        wp_register_script(
            $this->handle . '-search',
            UCONN_2019_CHILD_URL . $this->buildDir . '/search.js',
            [],
            false,
            true
        );   
    }
}
