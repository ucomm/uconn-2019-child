<?php

namespace UConn2019Child\Assets;

abstract class Loader
{
  protected $handle;
  protected $adminHandle;
  protected $parentStyleHandle;
  protected $parentScriptHandle;
  protected $buildDir;
  protected $parentBuildDir;

  public function __construct()
  {
    $this->handle = $this->setHandle();
    $this->adminHandle = $this->setAdminHandle();
    $this->parentScriptHandle = $this->setParentScriptHandle();
    $this->parentStyleHandle = $this->setParentStyleHandle();

    // set the build directory based on environment
    $this->buildDir = $_SERVER['HTTP_HOST'] === 'localhost' ?
      '/build' :
      '/dist';
    
    $this->parentBuildDir = $_SERVER['HTTP_HOST'] === 'localhost' ?
      '/build' :
      '/dist';
  }

  /**
   * Run the wp_enqueue_scripts action
   *
   * @return void
   */
  public function enqueueAssets()
  {
    add_action('wp_enqueue_scripts', [$this, 'enqueue']);
  }

  /**
   * Run the admin_enqueue_scripts action
   *
   * @return void
   */
  public function enqueueAdminAssets()
  {
    add_action('admin_enqueue_scripts', [$this, 'adminEnqueue']);
  }

  /**
   * Enqueue assets when the wp_enqueue_scripts action is called
   *
   * @return void
   */
  abstract function enqueue();

  /**
   * Enqueue assets when the admin_enqueue_scripts action is called
   *
   * @return void
   */
  abstract function adminEnqueue(string $hook);

  /**
   * Set the base handle for scripts and styles
   *
   * @return string
   */
  protected function setHandle(): string
  {
    return 'uconn-2019-child';
  }

  protected function setParentStyleHandle(): string {
    return 'uconn-2019-styles';
  }

  protected function setParentScriptHandle(): string {
    return 'uconn-2019-script';
  }

  /**
   * Set the base handle for admin scripts and styles
   *
   * @return string
   */
  protected function setAdminHandle(): string
  {
    return 'uconn-2019-child-admin';
  }
}
