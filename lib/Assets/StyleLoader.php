<?php

namespace UConn2019Child\Assets;

use UConn2019Child\Assets\Loader;

/**
 * A class to handle loading CSS assets
 */
class StyleLoader extends Loader
{
  /**
   * Enqueue the prepared scripts.
   * In this example, the script will only be enqueued if the shortcode is present.
   *
   * @return void
   */
  public function enqueue()
  {
    $this->dequeueStyles();
    $this->prepareThemeStyles();

    wp_enqueue_style($this->handle);
  }

  /**
   * Remove conflicting or unneeded styles
   * 
   * @return void
   */
  private function dequeueStyles() {
    $styles = [
      'uc_people-css',
      'papercite_css',
      'ucomm-banner-css-2'
    ];

    foreach ($styles as $style) {
      wp_dequeue_style($style);
    }
  }

  /**
   * Prepare the styles by registering them. The directory for the script is determined by the environment.
   *
   * @return void
   */
  private function prepareThemeStyles()
  {
    $styleDeps = [ $this->parentStyleHandle ];
    wp_register_style(
      $this->handle,
      UCONN_2019_CHILD_URL . $this->buildDir . '/main.css',
      $styleDeps,
    );
  }

  /**
   * This method can be used to enqueue an asset on an admin page.
   * Use the slug to filter which pages it should be used on.
   *
   * @param string $hook - the admin page's slug to enqueue on
   * @return void
   */
  public function adminEnqueue(string $hook)
  {
    $settingsPage = stripos($hook, 'boilerplate');
    if ($settingsPage !== false) {
      $this->prepareAdminStyles();
      wp_enqueue_style($this->adminHandle);
    }
  }

  private function prepareAdminStyles()
  {
    $styleDeps = [];
    return wp_register_style(
      $this->adminHandle,
      UCONN_2019_CHILD_URL . 'admin-styles.css',
      $styleDeps
    );
  }
}
