<?php

namespace UConn2019Child\Search;

use WP_Query;

class Search {
  /**
   * AJAX keyword search handler
   *
   * @return void
   */
  public static function searchHandler(): void {
    check_ajax_referer('search-nonce');
    $posts = [];
    $s = sanitize_text_field($_POST['s']);
    $paged = isset($_POST['paged']) ? $_POST['paged'] : 1;
    $query = new WP_Query([
      's' => $s,
      'paged' => $paged,
      'post_status' => 'publish'
    ]);

    if ($query->have_posts()) {

      while ($query->have_posts()) {
        $query->the_post();
        $data = [
          'id' => get_the_ID(),
          'title' => get_the_title(),
          'link' => get_the_permalink(),
          'excerpt' => get_the_excerpt(),
          'author' => get_the_author(),
          'image' => get_the_post_thumbnail_url()
        ];
        array_push($posts, $data);
      }
    }

    wp_send_json([
      's' => $s,
      'posts' => $posts
    ]);
    wp_die();
  }
}