<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <title><?php wp_title( '' ); ?></title>
    <?php
        // micro data for UConn see - schema.org and https://schema.org/EducationalOrganization
    ?>
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "EducationalOrganization",
            "name": "University of Connecticut",
            "address": {
                "@type": "PostalAddress",
                "addressRegion": "Connecticut",
                "telephone": "860-486-2000"
            },
            "url": "https://uconn.edu"
        }
    </script>
    <?php wp_head(); ?>
</head>
<body <?php 
    $classes = [ 'uc-2019-child' ];

    $isBBPage = has_block('fl-builder/layout');

    if (!$isBBPage) {
        array_push($classes, 'page-with-blocks');
    }
    body_class($classes); 
?>>
    
    <?php get_template_part('template-parts/cookie-notice'); ?>

    <a href="#main-content" class="skip-content-link skip-link">Skip to Content</a>

    <div class="wrapper">
        <?php get_template_part('template-parts/banner'); ?>

        <div id="main-navigation" class="navigation-desktop">
            <?php UConn2019\Lib\Menus::display_top_navigation(); ?>
        </div>

        <?php // Mobile nav is in template-parts/banner ?>
