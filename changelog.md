# Changelog
## 1.1.7
- Fix to remove static method calls
## 1.1.6
- Removed call for alert banner in `header.php`
## 1.1.5
- Updated CSS to use new variables and reduce dependency on tailwindcss
## 1.1.4
- Modified rendering of page title in header.php, as it's added to the site via plugin now
## 1.1.3
- Removed meta description from header.php, as it's added to the site via plugin
## 1.1.2
- Security fix. Sanitization of search query parameter
## 1.1.1
- Added button style for slate forms
## 1.1.0
- Improved search functionality
  - Added support for UConn custom google search and phone book
- Added typescript support
## 1.0.9
- Added links to footer for brand compliance
## 1.0.8
- Updated search button style (on search page)
## 1.0.7
- Added `uconn-2019-child` class to body
- Updated forminator submit button style
## 1.0.6
- Updated mobile nav underline color
## 1.0.5
- Flipped colors for links
## 1.0.4
- Update style for nav menu buttons
## 1.0.3
- Style update for menu. Child themes now use royal blue highlights

## 1.0.2
- Fixed banner 
  - to link to same-site search page
  - to have the name of the site in the tooltip
  - wordmark to home url for each site
- Updated search page 
  - more relevant results
  - using parent theme search results template part
- Fixed search page styles

## 1.0.1
- Removed unneeded `require` 

## 1.0.0
- Initial release