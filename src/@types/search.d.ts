// this type is based on the properties defined in the script loader
type CustomSearch = {
  cx: string,
  homeURL: string,
  requestURL: string,
  wpAJAX: string,
  wpSearchNonce: string,
}

type Post = {
  id: string,
  title: string,
  link: string,
  excerpt: string,
  author: string,
  image: string,
}