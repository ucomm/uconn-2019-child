let form: HTMLFormElement
let resultsList: HTMLElement
let searchMoreButton: HTMLButtonElement
let searchPaged: HTMLInputElement
let searchHeader: HTMLHeadingElement
let searchQueryText: HTMLElement
let cseResults: HTMLElement

// make sure that typescript knows about the global customSearch variable
declare var customSearch: CustomSearch

const resetForm = () => {
  form.reset()
  resultsList.innerHTML = ''
  searchQueryText.innerText = ''
  searchMoreButton.style.display = 'none'
  searchHeader.style.display = 'none'
  searchPaged.value = '1'
}

document.addEventListener('DOMContentLoaded', () => {

  const customSearchObj = customSearch

  const cx = typeof customSearch.cx === undefined ? '004595925297557218349:65_t0nsuec8' : customSearchObj.cx
  const themeScript = document.querySelector('#uconn-2019-child-js')
  const radios = <NodeListOf<HTMLInputElement>>document.querySelectorAll('#uconn-2019-search input[type=radio]')
  const searchInput = <HTMLInputElement>document.querySelector('#search-input')
  const submitButton = <HTMLButtonElement>document.querySelector('#search-submit')
  const peopleSearchWrapper = <HTMLElement>document.querySelector('#people-search-wrapper')
  const localSearchWrapper = <HTMLElement>document.querySelector('#local-search-wrapper')
  const uconnSearchWrapper = <HTMLElement>document.querySelector('#cse-search-results')
  const originalSearchPlaceHolder = searchInput.placeholder

  form = <HTMLFormElement>document.querySelector('#uconn-2019-search')
  resultsList = <HTMLElement>document.querySelector('#search-results')
  searchMoreButton = <HTMLButtonElement>document.querySelector('#search-more-button')
  searchPaged = <HTMLInputElement>document.querySelector('#search-paged')
  searchHeader = <HTMLHeadingElement>document.querySelector('#search-header')
  searchQueryText = <HTMLElement>document.querySelector('#search-query')
  cseResults = <HTMLElement>document.querySelector('#cse-search-results')
  
  radios.forEach(radio => {
    radio.addEventListener('click', () => {
      searchMoreButton.style.display = 'none'
      
      if (radio.checked && radio.id === 'search-local') {
        peopleSearchWrapper.classList.remove('show')
        peopleSearchWrapper.classList.add('hidden')
        uconnSearchWrapper.classList.remove('show')
        uconnSearchWrapper.classList.add('hidden')
        localSearchWrapper.classList.remove('hidden')
        localSearchWrapper.classList.add('show')
        searchInput.placeholder = originalSearchPlaceHolder
      } else if (radio.checked && radio.id === 'search-uconn') {
        peopleSearchWrapper.classList.remove('show')
        peopleSearchWrapper.classList.add('hidden') 
        localSearchWrapper.classList.remove('show')
        localSearchWrapper.classList.add('hidden')
        uconnSearchWrapper.classList.remove('hidden')
        uconnSearchWrapper.classList.add('show')
        searchInput.placeholder = 'Search UConn...'
      } else if (radio.checked && radio.id === 'search-people') {
        localSearchWrapper.classList.remove('show')
        localSearchWrapper.classList.add('hidden')
        uconnSearchWrapper.classList.remove('show')
        uconnSearchWrapper.classList.add('hidden')
        peopleSearchWrapper.classList.remove('hidden')
        peopleSearchWrapper.classList.add('show')
      }

      if (radio.checked && radio.id !== 'search-people') {
        searchInput.style.display = 'block'
        submitButton.style.display = 'block'
      } else {
        searchInput.style.display = 'none'
        submitButton.style.display = 'none'
      }

      if (radio.checked && radio.id === 'search-local') {
        searchInput.setAttribute('name', 's')
      } else {
        searchInput.setAttribute('name', 'q')
        searchPaged.value = '1'
      }
    })
  })

  searchInput.addEventListener('input', (evt: Event) => {
    let searchInput
    if (!evt || !evt.currentTarget) {
      return
    }

    if (evt.currentTarget) {
      searchInput = evt.currentTarget as HTMLInputElement
    }

    if (searchInput?.value === '') {
      resetForm()
    } else {
      searchPaged.value = '1'
    }
  })

  searchMoreButton.addEventListener('click', () => {
    // re-submit the form without page relod
    form.requestSubmit()
  })

  form.addEventListener('submit', (evt) => {
    const searchName = searchInput.getAttribute('name')
    if (searchName !== 's') {
      return
    }

    evt.preventDefault()

    cseResults.style.display = 'none'

    submitButton.setAttribute('disabled', '')
    submitButton.innerText = 'Searching...'

    const formData = new FormData(evt.target as HTMLFormElement)
    formData.append('_ajax_nonce', customSearch.wpSearchNonce)
    formData.append('action', 'search')

    fetch(customSearchObj.wpAJAX, {
      method: 'POST',
      body: formData
    }).then(res => res.json())
      .then(data => {
        const { posts, s }: { posts: Post[], s: string } = data

        posts.forEach(post => {
          const item = document.createElement('article')
          item.classList.add('search-result-item')
          item.id = `search-result-${post.id}`
          item.innerHTML = `
            <div class="post-content-wrapper">
              <div class="post-content-container">
                <div class="post-content">
                  <h3>
                    <a href="${post.link}">${post.title}</a>
                  </h3>
                  <p>${post.excerpt}</p>
                  <p>
                    <strong><a href="${post.link}" aria-label="Read more about ${post.title}">Read more</a></strong>
                  </p>
                </div>
              </div>
            </div>
          `
          resultsList.appendChild(item)
        });

        searchMoreButton.style.display = posts.length ? 'block' : 'none'
        searchHeader.style.display = 'block'
        searchQueryText.innerText = s
      })
      .catch(err => {
        console.error(err)
        const errorMessage = <HTMLElement>document.querySelector('#search-error')
        errorMessage.style.display = 'block'
      })
      .finally(() => {
        searchPaged.value = `${parseInt(searchPaged.value) + 1}`
        submitButton.removeAttribute('disabled')
        submitButton.innerText = 'Search'
      })
  })

  const gcse = document.createElement('script')
  gcse.id = 'uconn-gcse-script'
  gcse.type = 'text/javascript'
  gcse.async = true
  gcse.src = `https://www.google.com/cse/cse.js?cx=${cx}`

  if (themeScript && themeScript.parentNode) {
    <HTMLScriptElement>themeScript.parentNode.insertBefore(gcse, themeScript)
  }
})

document.addEventListener('keyup', (evt) => {
  if (evt.code !== 'Escape') {
    return
  }
  resetForm()
})