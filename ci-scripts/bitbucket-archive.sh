#!/bin/bash

# filename="uconn-2019-test"

composer archive --format=zip --file "./$FILENAME"

curl --location --request POST --user "${BITBUCKET_API_USERNAME}:${BITBUCKET_API_PASSWORD}" "https://api.bitbucket.org/2.0/repositories/ucomm/${BITBUCKET_SLUG}/downloads" \
--form files=@"./$FILENAME.zip"