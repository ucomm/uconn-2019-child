pipeline {
  agent any
  environment {
    THEME_SLUG = "uconn-2019-child"
    BITBUCKET_SLUG = "uconn-2019-child"
    DEV_BRANCH = "develop"
    FEATURE_BRANCH = "feature/**"
    PROD_BRANCH = "main"
  }
  // handle rsync dry runs
  parameters {
    booleanParam(
      name: 'DEPLOY_TO_PROD',
      defaultValue: false,
      description: 'In case the master branch needs to go to prod, this will toggle the prod push'
    )
    booleanParam(
      name: 'RSYNC_DRY_RUN',
      defaultValue: true,
      description: 'Toggles the rsync --dry-run flag'
    )
  }
  stages {
    stage('Checkout') {
      steps {
        // do not delete builds. there's a breaking issue there
        // https://issues.jenkins.io/browse/JENKINS-66843
        scmSkip(deleteBuild: false)
        sendNotifications 'STARTED'
      }
    }
    stage("Prepare Build Assets") {
      when {
        anyOf {
          branch "${DEV_BRANCH}";
          branch "${FEATURE_BRANCH}";
          branch "${PROD_BRANCH}";
          buildingTag()
        }
      }
      parallel {
        stage('NPM') {
          steps {
            sh "${WORKSPACE}/ci-scripts/npm.sh"
          }
        }
        stage('Composer') {
          steps {
            sh "${WORKSPACE}/ci-scripts/composer.sh"
          }
        }
      }
      post {
        always {
          echo "======== $BRANCH_NAME end asset builds ========"
        }
        success {
          echo "======== success - $BRANCH_NAME asset builds ========"
        }
        failure {
          echo "======== failed - $BRANCH_NAME asset builds ========"
        }
      }
    }
    stage('Dev Pushes') {
      environment {
        FILENAME = "$GIT_BRANCH"
        SITE_DIRECTORY = "edu.uconn.devredesign"
      }
      when {
        anyOf {
          branch "${DEV_BRANCH}";
          branch "${FEATURE_BRANCH}"
        }
      }
      parallel {
        stage('Push to staging0') {
          steps {
            sh "${WORKSPACE}/ci-scripts/dev-push.sh"
          }
        }
        stage('Archive dev to bitbucket') {
          steps {
            script {
              if (env.BRANCH_NAME == "${DEV_BRANCH}") {
                sh "${WORKSPACE}/ci-scripts/bitbucket-archive.sh"
              }
            }
          }
        }
      }
    }
    stage('Staging Push') {
      environment {
        SITE_DIRECTORY = "edu.uconn.staging"
      }
      when {
        branch "${PROD_BRANCH}"
      }
      steps {
        sh "${WORKSPACE}/ci-scripts/dev-push.sh"
      }
      post {
        failure {
          sendNotifications("FAILURE - Push to staging")
        }
      }
    }
    stage('Prod Push') {
      environment {
        SITE_DIRECTORY = "edu.uconn"
        FILE_NAME = "${TAG_NAME}"
      }
      when {
        anyOf {
          buildingTag()
          expression {
            return params.DEPLOY_TO_PROD && env.BRANCH_NAME == "${PROD_BRANCH}"
          }
        }
      }
      parallel {
        stage('Push tag to prod') {
          steps {
            sh "${WORKSPACE}/ci-scripts/prod-push.sh"
          }
        }
        stage('Archive tag to bitbucket') {
          steps {
            script {
              if (env.RSYNC_DRY_RUN == "false") {
                sh "${WORKSPACE}/ci-scripts/bitbucket-archive.sh"
              }
            }
          }
        }
      }
      post {
        failure {
          sendNotifications("FAILURE - Push to prod")
        }
        success {
          script {
            if (env.RSYNC_DRY_RUN == "false") {
              sendNotifications("SUCCESS", "#comm0-updates", [
                [
                  type: "section",
                  text: [
                    type: "mrkdwn",
                    text: ":tada: *$THEME_SLUG* updated"
                  ]
                ]
              ])
              slackUploadFile(channel: "#comm0-updates", filePath: "changelog.md", initialComment:  "uconn-2019 Changelog")
            }
          }
        }
      }
    }
  }
  post {
    success {
      sendNotifications 'SUCCESSFUL'
    }
    failure {
      sendNotifications 'FAILED'
    }
    aborted {
      sendNotifications 'ABORTED'
    }
    always {
      echo "======== Cleanup ========"
      sh "rm -rf node_modules"
    }
  }
}