<?php

$pages = get_pages();

$pageIDs = array_map(function ($page) {
  return $page->ID;
}, $pages);

$selectedPageIDs = array_map(function ($ID) {
  return intval($ID);
}, $opts['boilerplate-array']);

$intersection = array_intersect($pageIDs, $selectedPageIDs);

?>

<div class="container">
  <div>
    <label for="text-input">Example Text Input</label>
  </div>
  <div>
    <input id="text-input" type="text" name="<?php echo $this->settingsName; ?>[boilerplate-string]" value="<?php echo $settings['boilerplate-string']; ?>">
  </div>
  <div class="container">
    <div>
      <label for="select-input">Example Select</label>
    </div>
    <div>
      <select name="<?php echo $this->settingsName; ?>[boilerplate-array][]" id="select-input">
        <option value="" disabled="true">---</option>
        <?php
        foreach ($pages as $index => $page) {
          $selected = in_array($page->ID, $intersection) ?
            'selected="selected"' :
            '';
        ?>
          <option value="<?php echo $page->ID; ?>" <?php echo $selected; ?>><?php echo $page->post_title; ?></option>
        <?php
        }
        ?>
      </select>
    </div>
  </div>
</div>