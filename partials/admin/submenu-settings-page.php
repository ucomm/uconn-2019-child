<div class="wrap">
  <h1><?php echo esc_html(get_admin_page_title()); ?></h1>
  <div>
    <form id="goilerplate-settings-form" action="options.php" method="POST">
      <?php
      settings_fields('boilerplate-settings');

      do_settings_sections('boilerplate-settings');
      ?>
      <div>
        <?php submit_button('Update Settings'); ?>
      </div>
    </form>
  </div>
</div>