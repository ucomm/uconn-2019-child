<?php

use UConn2019Child\Frontend\SiteManagement;

global $wp;

// $q is the required param for the custom search engine as opposed to WP default which is 's'
$q = isset($_GET['q']) ? $_GET['q'] : '';
$info = SiteManagement::getSiteInfo(['description']);

$radios = [
  (object) [
    'name' => __($info['description'], 'uconn-2019-child'),
    'id' => 'search-local',
    'value' => 'local'
  ],
  (object) [
    'name' => __('UConn', 'uconn-2019-child'),
    'id' => 'search-uconn',
    'value' => '004595925297557218349:65_t0nsuec8'
  ],
  (object) [
    'name' => __('People', 'uconn-2019-child'),
    'id' => 'search-people',
    'value' => 'people'
  ]
]

?>

<form id="uconn-2019-search" method="GET" action="<?php echo add_query_arg($wp->query_string, '', home_url($wp->request)); ?>">
  <div>
    <div class="search-radio-wrapper">
      <?php
      foreach ($radios as $r) {
      ?>
        <label for="<?php echo $r->id; ?>">
          <input type="radio" name="cx" id="<?php echo $r->id; ?>" class="search-radio" value="<?php echo $r->value; ?>" <?php 
            '' !== $q ? checked($r->value, '004595925297557218349:65_t0nsuec8') : checked($r->value, 'local');          
          ?>>
          <?php echo $r->name; ?>
        </label>
      <?php
      }
      ?>
    </div>
    <div>
      <input placeholder="Search <?php echo $args['description']; ?>..." aria-label="search <?php echo $args['description']; ?>" type="search" id="search-input" name="<?php 
        echo $q !== '' ? 'q' : 's';
      ?>" value="<?php echo $q; ?>">
      <input type="hidden" name="paged" value="1" id="search-paged">
    </div>
  </div>
  <div>
    <button id="search-submit" type="submit" value="">Search</button>
  </div>
</form>