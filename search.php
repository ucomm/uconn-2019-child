<?php

use UConn2019Child\Frontend\Search;

add_filter('body_class', array('UConn2019\Lib\Helpers', 'add_angled_header_class'));
get_header();

$args = Search::createSearchFormArgs();

if (!isset($helpers) || !class_exists('UConn2019\Lib\Helpers')) {
  include get_template_directory() . '/lib/Helpers.php';
  $helpers = new \UConn2019\Lib\Helpers();
}

?>

<main role="main" id="main-content">
  <?php echo $helpers->get_angled_header('Search'); ?>
  <section class="search-contain">
    <h2>
      Search Results for <?php echo wp_kses($args['value'], 'strip'); ?>
    </h2>
    <?php
      get_search_form($args);
      include get_template_directory() . '/template-parts/content-search.php';
    ?>
  </section>
</main>

<?php

get_footer();
