# UConn 2019 Child/Regional Campus Theme

This project is intended to be used to develop and maintain a child theme of [uconn-2019](https://bitbucket.org/ucomm/uconn-2019/src/master/). The goal is to allow the regional campus sites to move in sync with the styles for the main [uconn.edu](https://uconn.edu) site. 

## Get Started

- clone this repo
- `git flow init -d`
- `composer install`
- `composer update`
- `nvm use`
- `npm install`
- create the initial WP database

Set up the uconn-2019 theme

- `cd www/content/themes/uconn-2019`
- `composer install --no-dev`
- `cd` to project root

Start developing (use two terminals or docker in daemon mode)

- `docker-compose up [-d]`
- `nvm use`
- `npm run dev`

If you want the current uconn.edu database

- connect to the db on comm0 (ask for credentials)
- export all tables _except_ the `wp_users` and `wp_usermeta` tables
- import the tables to the local db
- use the [Better Search Replace](https://wpackagist.org/search?q=better-search-replace&type=any&search=) plugin to fix urls in the db
- use the [bb-data-fix](https://bitbucket.org/ucomm/bb-data-fix/src/main/) plugin. BB tends to be a pain about search and replace updates 


### Local asset development

**Deprecations**
- Building assets in a docker container. It turned out to be a terrible developer experience. See the steps above for launching webpack.
- IE. 

**Supports**
- sass
- [TailwindCSS](https://tailwindcss.com/)
- typescript

Locally, assets are unminified and part of the repo in the `/build` directory. We do this to make it easier to pull the project into other local environments.

For production builds, they are minified and in the `/dist` directory. See the `webpack.config.js` file for details.

### Viewing a project.
This project will be available at [localhost](http://localhost).

## Debugging and Testing

### Debugging
This repo allows debugging with the [VSCode PHP Debug extension](https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-debug). A sample `launch.json` file is included in the repo for reference. Create a new `launch.json` file, copy/paste the contents and edit them to match the name of the theme. You will then be able to run xdebug! 

Need more detail? The xdebug functionality is provided via [our custom docker image](https://bitbucket.org/ucomm/docker-images/src/master/Dockerfile-wpcore-xdebug) based on the official WordPress docker image.

### Debugging Docker

You can access Docker containers directly from the shell (as long as they are running), example:

```bash
docker-compose exec web bash
```

## Adding Plugins/Dependencies

This project uses composer as a dependency management tool for PHP.  It also uses NPM/Yarn as a dependency management tool for local build tools, to compile/minify CSS/JS.

Packages can be found at:

- [Composer Packagist](https://packagist.org/)
- [WordPress Packagist](https://wpackagist.org/)
- [NPM Packages (JavaScript)](https://www.npmjs.com/)

To add one of our plugins, themes or other PHP dependencies, use the following format in the `composer.json` file

```json
{
  "repositories": [
      {
          "type": "composer",
          "url": "https://wpackagist.org"
      },
      {
        "type": "vcs",
        "url": "git@bitbucket.org/ucomm/{dependency-name}"
      }
  ]
}
```

Then run

```bash
composer require (--dev) ucomm/{dependency-name}
```

Yes, this makes the repository list really long, but makes updates really fast.

## Git
We use git. Please check that the default branch name for `git init` is set to `main`. You can confirm this by looking at your `gitconfig` file at `~/.gitconfig`. It should look like this

```bash
[init]
	defaultBranch = main
```

If it doesn't, please either add it there or use the following command
```
git config --global init.defaultBranch main
```

## Branching Strategy
### git flow
We try to use [Git Flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) as our branching strategy.  Not required, but helpful when working on a team.

If you are using git flow, please set the default git flow branch name to main as well. Your `gitconfig` file, should look like this

```bash
[gitflow "branch"]
	master = main
```

Or run 

```
git config --global gitflow.branch.master main
```
### Tagging
Tags must follow the [semver system](http://semver.org/). Tags can be added in git flow using the `git flow release start` and `git flow release finish` commands.

## CI/CD

### Jenkins builds

In the Jenkins shell script, to create assets managed by webpack use the following format

```bash
# select the correct node version
source ~/.nvm/nvm.sh
nvm use

# install dependencies quickly
npm ci

# build assets
npm run build

# remove node_modules folders
find . -name "node_modules" -type d -exec rm -rf '{}' +
```

### Automating builds
In order to ensure automatic pushes to our development server(s) take the following steps
- Create a new [Jenkins project](http://ci.pr.uconn.edu:8080/) (either from scratch or by copying - see the [Castor project](http://ci.pr.uconn.edu:8080/job/Castor%20-%20Push%20to%20Dev%20(Aurora%20Sandbox)/) for an example)
- In bitbucket settings, go to `Settings -> Workflow -> Webhooks` and add a hook to Jenkins at the url `http://ci.pr.uconn.edu:8080/bitbucket-hook/`.
- In bitbucket settings, go to `Settings -> General -> Access Keys` and add the Jenkins ssh key. The key can be copy/pasted from another repo
- **If this project is going to be deployed to either the Aurora sandbox or health dev servers, make a new directory with the same name as the project using an ftp client. Otherwise the deployment may fail.**

### Bitbucket

On pushes to the `develop` or versioned `main` branch, bitbucket pipelines will build/bundle assets. It will the create a zip file of the project and add it to the [repo downloads area](https://bitbucket.org/ucomm/uconn-2019-child/downloads/). Zip file include/exclude configuration is done in the `composer.json` file. Note that it uses a gitignore style syntax. 
